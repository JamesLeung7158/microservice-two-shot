import { Link, useLocation, useNavigate } from 'react-router-dom';


export default function ShoeDelete(props) {
    let { state } = useLocation();

    const navigate = useNavigate();

    const handleDelete = async (event) => {
        event.preventDefault();

        const shoeUrl = `http://localhost:8080${state}`
        const fetchConfig = {
            method: "delete",
        }

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            console.log(response.deleted, response.breakdown)
            navigate('/shoes')
        }
    }

    return (
        
        <div>
            <h1>Are you sure?</h1>
            <form 
            onSubmit={handleDelete} 
            className="my-5 container"
            >
                <button className="btn btn-warning">
                    Yes
                </button>
            </form>
            <form className="my-5 container">
                <Link
                to="/shoes"
                className="btn btn-secondary"
                >
                    No
                </Link>
            </form>
        </div>
    )
};