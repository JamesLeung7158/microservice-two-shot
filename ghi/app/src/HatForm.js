import { useEffect, useState } from "react";

function HatForm() {
    const [locations, setLocations] = useState([]);
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [fabric, setFabric] = useState('');
    const [location, setLocation] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.fabric = fabric;
        data.style_name = style;
        data.color = color;
        data.image_url = picture;
        data.location = location;
        console.log(data);
        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log('new hat:', newHat)
        setStyle('');
        setColor('');
        setPicture('');
        setFabric('');
        setLocation('');
        }
    };



    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFabricChange} value={fabric}placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                        <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStyleChange} value={style}placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                        <label htmlFor="style">Style</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color}placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} value={picture}placeholder="Picture URL" required type="url" name="picture" id="picture" className="form-control" />
                        <label htmlFor="picture">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select
                    onChange={handleLocationChange}
                    value={location}
                    required
                    name="location"
                    id="location"
                    className="form-select"
                    >
                    <option value="">Choose a location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.href} value={location.href}>
                                    {location.closet_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create Hat</button>
                </form>
            </div>
            </div>
        </div>
    )
}

export default HatForm;
