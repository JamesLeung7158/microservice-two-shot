import React, { useEffect, useState } from 'react';

function HatList() {
    const[hats, setHats] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
            console.log(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (event) => {
        const hatUrl = `http://localhost:8090${event}`
        const fetchConfig = {
            method: "delete",
        }
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Picture</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {hats.map((hat, index) => {
                    return (
                        <tr className='fw-bold' key={hat.fabric + index}>
                            <td className='fs-3'>{ hat.fabric }</td>
                            <td className='fs-3'>{ hat.style_name }</td>
                            <td className='fs-3'>{ hat.color }</td>
                            <td>
                                <div className='fs-3'>{ hat.location.closet_name }</div>
                                <div>Section Number: {hat.location.section_number}</div>
                                <div>Shelf Number: {hat.location.shelf_number}</div>
                            </td>
                            <td><img className="rounded img-thumbnail img-fluid" height="200px" width="200px" src={ hat.image_url }/> </td>
                            <td>
                                <button onClick={() => handleDelete(hat.href)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}

export default HatList
