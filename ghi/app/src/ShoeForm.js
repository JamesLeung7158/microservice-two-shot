import React, { useEffect, useState } from 'react';

function ShoeForm(props) {
    const [bins, setBins] = useState([]);

    const [shoeManufacturer, setShoeManufacturer] = useState('');
    const handleShoeManufacturerChange = (event) => {
        const value = event.target.value;
        setShoeManufacturer(value);
    }

    const [shoeModel, setShoeModel] = useState('');
    const handleShoeModelChange = (event) => {
        const value = event.target.value;
        setShoeModel(value);
    }

    const [shoeColor, setShoeColor] = useState('');
    const handleShoeColorChange = (event) => {
        const value = event.target.value;
        setShoeColor(value);
    }

    const [shoePicture, setShoePicture] = useState('');
    const handleShoePictureChange = (event) => {
        const value = event.target.value;
        setShoePicture(value);
    }

    const [shoeBin, setShoeBin] = useState('');
    const handleShoeBinChange = (event) => {
        const value = event.target.value;
        setShoeBin(value);
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/"
        
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data, 'data')
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.shoe_manufacturer = shoeManufacturer;
        data.shoe_model = shoeModel;
        data.shoe_color = shoeColor;
        data.shoe_url = shoePicture;
        data.bin = shoeBin;
        console.log(data, 'json');

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json'
            }
        }

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe, 'New Shoe');

            setShoeManufacturer('');
            setShoeModel('');
            setShoeColor('');
            setShoePicture('');
            setShoeBin('');
        }

    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input 
                    onChange={handleShoeManufacturerChange} 
                    value={shoeManufacturer} 
                    placeholder="Manufacturer" 
                    required type="text" 
                    name="manufacturer" 
                    id="manufacturer" 
                    className="form-control"
                    />
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input 
                    onChange={handleShoeModelChange} 
                    value={shoeModel} 
                    placeholder="Model" 
                    required type="text" 
                    name="model" 
                    id="model" 
                    className="form-control"
                    />
                    <label htmlFor="model">Model</label>
                </div>
                <div className="form-floating mb-3">
                    <input 
                    onChange={handleShoeColorChange} 
                    value={shoeColor} 
                    placeholder="Color" 
                    required type="text" 
                    name="color" 
                    id="color" 
                    className="form-control"
                    />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input 
                    onChange={handleShoePictureChange} 
                    value={shoePicture} 
                    placeholder="Picture" 
                    required type="url" 
                    name="picture" 
                    id="picture" 
                    className="form-control"
                    />
                    <label htmlFor="picture">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select 
                    onChange={handleShoeBinChange} 
                    value={shoeBin} 
                    required 
                    name="bin" 
                    id="bin" 
                    className="form-select"
                    >
                    <option value="">Choose a bin</option>
                        {bins.map(bin => {
                            return (
                                <option key={bin.href} value={bin.href}>
                                    {bin.closet_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Add Shoe</button>
                </form>
            </div>
            </div>
        </div>
    )
};

export default ShoeForm;