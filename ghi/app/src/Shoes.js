import { useEffect, useState} from 'react';
import { Link } from 'react-router-dom';


export default function ShoesList () {
    const [shoes, setShoes] = useState([]);
    
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }
        
    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (event) => {

        const shoeUrl = `http://localhost:8080${event}`
        const fetchConfig = {
            method: "delete",
        }

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            console.log(response.deleted, response.breakdown)
            fetchData();
        }
    }

    return (
        <div className='my-5 container'>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Manufacturer</th>
                            <th>Model</th>
                            <th>Color</th>
                            <th>Bin</th>
                            <th>Picture</th>
                            <th>Remove?</th>
                        </tr>
                    </thead>
                    <tbody>
                        {shoes.map((shoe, index) => {
                            return (
                            <tr key={shoe.shoe_model + index}>
                                <td>{shoe.shoe_manufacturer}</td>
                                <td>{shoe.shoe_model}</td>
                                <td>{shoe.shoe_color}</td>
                                <td>
                                    <div>
                                        {shoe.bin.closet_name} Bin
                                        - Bin Number: {shoe.bin.bin_number}
                                        - Bin Size: {shoe.bin.bin_size}
                                    </div>
                                </td>
                                <td>
                                    <img 
                                    src={shoe.shoe_url} 
                                    alt={shoe.shoe_model} 
                                    className='img-thumbnail' 
                                    height="200" 
                                    width="200"
                                    />
                                </td>
                                
                                <td>
                                    {/* <Link 
                                    to= "delete"
                                    state = {shoe.href}
                                    className="btn btn-primary btn-sm px-4 gap-3"
                                    >
                                        Remove shoe
                                    </Link> */}
                                    <button 
                                    onClick={() => handleDelete(shoe.href)}
                                    className="btn btn-danger"
                                    >
                                        Remove
                                    </button>
                                </td>
                                
                            </tr>
                            )
                        })}
                    </tbody>
                </table>
                {/* {shoes.map((shoe, index) => {
                    return (
                    <div key={shoe.shoe_model + index}>
                        <div className='nav-item'>
                            <div>
                                {shoe.shoe_manufacturer} {shoe.shoe_model} - {shoe.shoe_color}
                            </div>
                            <div>
                                {shoe.bin.closet_name} Bin
                                - Bin Number: {shoe.bin.bin_number}
                                - Bin Size: {shoe.bin.bin_size}
                            </div>
                            
                        </div>
                        <img 
                        src={shoe.shoe_url} 
                        alt={shoe.shoe_model} 
                        className='img-thumbnail' 
                        height="200" 
                        width="200"
                        />
                    </div>
                    )
                })} */}
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                    <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add a shoe</Link>
                </div>
            </div>
        </div>
    )
}