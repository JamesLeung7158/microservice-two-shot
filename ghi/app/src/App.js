import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './Shoes';
import ShoeForm from './ShoeForm';
import ShoeDelete from './ShoeDelete';
import HatList from './HatList';
import HatForm from './HatForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="my-5 container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='shoes' >
            <Route path='' element={<ShoesList />} />
              <Route path='delete' element={<ShoeDelete />} />
            <Route path='new' element={<ShoeForm />}/>
          </Route>
          <Route path="hats">
            <Route path="" element={<HatList />} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
