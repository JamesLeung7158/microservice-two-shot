# Wardrobify

Team:

* Person 1 - James Leung - Shoes
* Person 2 - Kevin Ghiringhelli - Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

I plan on creating a shoe model with its manufacturer, its model name, its color, a URL for a picture, and the bin in the wardrobe where it exists.
Then I can have the wardrobe microservice request(s) some data from the bin, where the bin then sends back a response containing the shoe data specific to the request(s).

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Inside the hats model I will store the fabric, style name, color, picture url, along with the location in the wardrobe.  In order to obtain location data from the Wardrobe API, I will create a poller that pulls new locations and stores them in a value object.  That VO will allow me to select a location for the hat whenever I create a new one
