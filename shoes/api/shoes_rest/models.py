from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.closet_name}"

class Shoe(models.Model):
    shoe_manufacturer = models.CharField(max_length=200)
    shoe_model = models.CharField(max_length=200)
    shoe_color = models.CharField(max_length=200)
    shoe_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return f"{self.shoe_color} {self.shoe_model}"
    class Meta:
        ordering = ("shoe_model",)
    
    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})