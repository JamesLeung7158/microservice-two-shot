from django.contrib import admin
from .models import Location
from .models import Bin

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    pass


# Register your models here.
@admin.register(Bin)
class BinAdmin(admin.ModelAdmin):
    pass
